package account;

public class Bank {

    public void income(Account account, double amount) {
        Operation inc = new Income(account, amount);
        account.doOperation(inc);
    }

    public void transfer(Account from, Account to, double amount) {
        Operation trs = new Transfer(from, to, amount);
        from.doOperation(trs);
    }
}

	

