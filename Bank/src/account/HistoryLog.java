package account;

import java.util.Date;

public class HistoryLog {

	private Date dateOfOperation;
	private String title;
	private OperationType op;


	public HistoryLog(Date date, String title, OperationType op) {
		this.dateOfOperation = date;
		this.title = title;
		this.op = op;
	}

	public Date getDateOfOperation() {
		return dateOfOperation;
	}

	public String getTitle() {
		return title;
	}

	public OperationType getOp() 
	{
		return op;
	}

	@Override
	public String toString() {
		return dateOfOperation + " - " + title;
	}
}


