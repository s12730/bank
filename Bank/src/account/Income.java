package account;

import java.util.Date;

public class Income extends Operation {

	private Account account;
	private double amount;

	public Income(Account account, double amount) {
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void execute() {
		account.add(amount);
		HistoryLog hl = new HistoryLog(new Date(), amount + "PLN added to " + account, OperationType.INCOME);
	}
}
