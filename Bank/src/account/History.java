package account;

import java.util.ArrayList;
import java.util.List;

public class History {
	private List<HistoryLog> historyLog = new ArrayList<HistoryLog>();

	public void log(HistoryLog inLog) {
		this.historyLog.add(inLog);
	}

	@Override
	public String toString() {
		return historyLog.toString();
	}
}