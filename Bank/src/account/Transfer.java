package account;

import java.util.Date;

public class Transfer extends Operation {

	private Account from;
	private Account to;
	private double amount;
	
	
	public Transfer(Account from, Account to, double amount) {
		this.from = from;
		this.to = to;
		this.amount = amount;
	}

	@Override
	public void execute() {
		from.subtract(amount);
		to.add(amount);
		HistoryLog hl = new HistoryLog(new Date(), amount + "PLN transferred from " + from + " to " + to, OperationType.TRANSFER);
		from.getHistory().log(hl);
		to.getHistory().log(hl);
	}
}
