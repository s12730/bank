package account;

public class Main {

	public static void main(String[] args) 
	{
		   Bank bank = new Bank();
	        Account a = new Account("ACC:A01");
	        Account b = new Account("ACC:B01");
	        bank.income(a, 1000);
	        bank.income(b, 500);
	        bank.transfer(b, a, 300);
	        bank.transfer(a, b, 200);
	        System.out.println(a.getAmount() + " " + a.getHistory());
	        System.out.println(b.getAmount() + " " + b.getHistory());

	    }

	}


