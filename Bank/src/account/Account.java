package account;

public class Account {

    private double amount;
    private String number;
    private History history;

    public Account(String number) {
        this.number = number;
        this.amount = 0;
        this.history = new History();
    }

    public void subtract(double amount) {
        this.amount -= amount;
    }

    public void add(double amount) {
        this.amount += amount;
    }

    public void doOperation(Operation op) {
        op.execute();
    }

    public History getHistory() {
        return history;
    }


    public String getNumber() {
        return number;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return this.getNumber();
    }
}